# Build 

run command: `gradlew bootJar`

the jar can be found at: `build/libs/rs-audit-upload.jar`

# Parameters

A tool for uploading files to the `earp.rsi.ge`

for bulk upload several files, specify the directory path containing the files as `--file` parameter. 


`--file`      _-  the file to upload or get status for_

`--status`    _-  check status for the file_

`--username`  _-  username_

`--password`  _-  password_

`--rsi.upload.base-url`- _default: `https://earp.rsi.ge/`_

#### USAGES:

for bulk upload:

`java -jar rs-audit-upload.jar --file=file:files-directory/ --username=USERNAME --password=PASSWORD`

for statuses:

`java -jar rs-audit-upload.jar --file=file:files-directory/ --username=USERNAME --password=PASSWORD --status`


for single file upload:

`java -jar rs-audit-upload.jar --file=file:lone-sad-file.xml --username=USERNAME --password=PASSWORD`


for statuses:

`java -jar rs-audit-upload.jar --file=file:lone-sad-file.xml --username=USERNAME --password=PASSWORD --status`
