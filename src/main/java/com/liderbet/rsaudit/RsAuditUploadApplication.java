package com.liderbet.rsaudit;

import com.fasterxml.jackson.databind.JsonNode;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.ExitCodeExceptionMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.reactive.function.client.WebClientCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.UrlResource;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.Exceptions;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;
import reactor.netty.transport.logging.AdvancedByteBufFormat;

import javax.net.ssl.SSLException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.*;
import java.util.*;
import java.util.function.Function;
import java.util.stream.BaseStream;
import java.util.stream.Stream;

@SpringBootApplication
public class RsAuditUploadApplication {
    public static void main(String[] args) throws Throwable {
        SpringApplication.run(RsAuditUploadApplication.class, args);
    }

    @Bean
    WebClientCustomizer createWebClient(Props props) throws SSLException {
        return webClientBuilder -> {
            SslContext sslContext = null;
            try {
                sslContext = SslContextBuilder
                        .forClient()
                        .trustManager(InsecureTrustManagerFactory.INSTANCE)
                        .build();
            } catch (SSLException e) {
                e.printStackTrace();
            }
            SslContext finalSslContext = sslContext;
            webClientBuilder.clientConnector(new ReactorClientHttpConnector(HttpClient.create()
                    .wiretap("reactor.netty.http.client.HttpClient",
                            LogLevel.DEBUG, AdvancedByteBufFormat.TEXTUAL)
                    .secure(t -> t.sslContext(finalSslContext))))
                    .baseUrl(props.getBaseUrl());
        };
    }

    @Bean
    WebClient webClient(WebClient.Builder builder) {
        return builder.build();
    }

    @Bean
    ApplicationRunner applicationRunner(WebClient webClient) {
        return args -> {

            try {
                UrlResource file =
                        Stream.of(args.getOptionValues("f"),
                                args.getOptionValues("file"))
                                .filter(Objects::nonNull)
                                .flatMap(Collection::stream)
                                .findFirst()
                                .map(this::getUrlResource)
                                .orElseThrow(() -> new IllegalArgumentException("either --file option must be present"));

                if (!args.containsOption("password")) {
                    throw new IllegalArgumentException("password must be provided");
                }

                if (!args.containsOption("username")) {
                    throw new IllegalArgumentException("username must be provided");
                }


                Function<Mono<String>, Mono<List<JsonNode>>> action = this::uploadFile;

                if (args.containsOption("status") || args.containsOption("s")) {
                    action = this::checkStatus;
                }



                login(args.getOptionValues("username").get(0),
                        args.getOptionValues("password").get(0))
                        .transform(action)
                        .flatMapIterable(jsonNodes -> jsonNodes)
                        .doOnNext(jsonNode -> {
                            System.out.println("result: ");
                            System.out.println(jsonNode.toPrettyString());
                        })
                        .contextWrite(context -> context
                                .put(WebClient.class, webClient)
                                .put(UrlResource.class, file))
                        .blockLast();
            } catch (IllegalArgumentException e) {
                System.err.println(e.getMessage());
                System.err.flush();
                System.err.println();
                System.out.println();
                System.out.println("--file      -  the file to upload or get status for");
                System.out.println("--status    -  check status for the file");
                System.out.println("--username  -  username");
                System.out.println("--password  -  password");
                throw e;
            }
        };
    }

    @Bean
    ExitCodeExceptionMapper exitCodeToexceptionMapper() {
        return exception -> {
            // set exit code based on the exception type
            if (exception.getCause() instanceof IllegalArgumentException) {
                return -1;
            }
            return -10;
        };
    }

    private Mono<List<JsonNode>> uploadFile(Mono<String> token) {
        PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:**/*.xml");
        Flux<Path> files = Mono.deferContextual(contextView -> Mono.just(contextView.get(UrlResource.class)))
                .map((urlResource) -> {
                    try {
                        return urlResource.getFile().toPath();
                    } catch (IOException e) {
                        throw Exceptions.propagate(e);
                    }
                }).flatMapMany(path -> {
                    if (Files.isDirectory(path)) {
                        return Flux.using(() -> Files.walk(path, 1), Flux::fromStream, BaseStream::close)
                                .filter(matcher::matches);
                    }
                    return Mono.just(path);
                });
        return token.flatMap(s ->
                Mono.deferContextual(contextView -> files
                        .doOnNext(path -> System.out.println(path))
                        .flatMap(path -> {
                            try {
                                return contextView
                                        .get(WebClient.class)
                                        .post()
                                        .uri("/api/file/uploadfile")
                                        .contentType(MediaType.MULTIPART_FORM_DATA)
                                        .header("x-access-token", s)
                                        .body(BodyInserters
                                                .fromMultipartData("report", new UrlResource(path.toUri())))
                                        .exchangeToMono(clientResponse -> clientResponse.bodyToMono(JsonNode.class));
                            } catch (MalformedURLException e) {
                                return Mono.error(e);
                            }
                        }).collectList()
                ));
    }

    private Mono<List<JsonNode>> checkStatus(Mono<String> token) {

        PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:**/*.xml");


        Flux<Path> files = Mono
                .deferContextual(contextView -> Mono.just(contextView.get(UrlResource.class)))
                .map((urlResource) -> {
                    try {
                        return urlResource.getFile().toPath();
                    } catch (IOException e) {
                        throw Exceptions.propagate(e);
                    }
                }).flatMapMany(path -> {
                    if (Files.isDirectory(path)) {
                        return Flux.using(() -> Files.walk(path, 1), Flux::fromStream, BaseStream::close)
                                .filter(matcher::matches);
                    }
                    return Mono.just(path);
                });


        return files.flatMap(path -> token.flatMap(s ->
                Mono.deferContextual(contextView ->
                        contextView.get(WebClient.class).get().uri("/api/file/{fileName}", path.getFileName())
                                .header("x-access-token", s)
                                .exchangeToMono(clientResponse -> clientResponse.bodyToMono(JsonNode.class))))).collectList();

    }

    private Mono<String> login(String username, String password) {
        return Mono.deferContextual(contextView ->
                contextView.get(WebClient.class).post()
                        .uri("/api/auth/login")
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .body(BodyInserters
                                .fromFormData("username", username)
                                .with("password", password))
                        .exchangeToMono(clientResponse -> clientResponse.bodyToMono(JsonNode.class))
                        .map(jsonNode -> jsonNode.get("token").asText()));
    }

    private UrlResource getUrlResource(String s) {
        try {
            return new UrlResource(s);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }


}
