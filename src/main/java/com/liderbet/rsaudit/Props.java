package com.liderbet.rsaudit;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("rsi.upload")
@Data
public class Props {
    String baseUrl="https://earp.rsi.ge/";
}
